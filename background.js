let capturedData = []; // Store captured requests
let analyticsHost = ""; // Default value or leave empty
let devToolsConnections = [];

chrome.storage.sync.get(["analyticsHost"], (result) => {
  analyticsHost = result.analyticsHost;
});

chrome.webRequest.onBeforeRequest.addListener(
  (details) => {
    if (details.url.includes(analyticsHost)) {
      console.log("Captured request:", details);
      capturedData.push(details);
      broadcastData();
    }
  },
  { urls: ["<all_urls>"] },
  ["requestBody"]
);

chrome.storage.onChanged.addListener(function (changes, namespace) {
  for (let [key, { oldValue, newValue }] of Object.entries(changes)) {
    if (key === "analyticsHost") {
      analyticsHost = newValue;
      capturedData = [];
      broadcastData();
    }
  }
});

chrome.runtime.onConnect.addListener((port) => {
  if (port.name === "matomoTrackerDevTools") {
    devToolsConnections.push(port);
    port.onDisconnect.addListener(() => {
      devToolsConnections = devToolsConnections.filter(
        (activePort) => activePort !== port
      );
    });
    port.onMessage.addListener((msg) => {
      if (msg.action === "clearData") {
        capturedData = []; // Clear captured data
        broadcastData(); // Update all connected devtools panels
      }
    });
    port.postMessage({ action: "sendData", data: capturedData });
  }
});

function broadcastData() {
  devToolsConnections.forEach((port) => {
    port.postMessage({ action: "sendData", data: capturedData });
  });
}
