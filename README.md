# Digital Theatre+ Matomo Analytics Event Viewer

## Chrome Extension

This Chrome Extension is not distributed via the Chrome Webstore.  You can download the project as a zip file, extract it onto your computer, and use Chrome to load the extension "unpacked".

To use:

1. Click the Code dropdown above and select zip (under Download source code)
2. Extract the contents of the zip file into a new folder on your computer
3. Open chrome://extensions/
4. Enable Developer mode (toggle, top right)
5. Select Load unpacked and select the folder you extracted the source code to
6. Pin the Digital Theatre+ Matomo Analytics Tracker extension
7. Click the extension and enter the Analytics hostname
8. Open Dev Tools and view the Matomo Tracker tab
9. Browse the Digital Theatre+ website
10. View the events being emitted (including descriptions of the cryptic parameter names)

![Screenshot showing Matomo Analytics Events](screenshot.png)

## Known issues

1. Chrome is unable to detect the payload / request body of POST events, so this only works with parameters in the URL
2. Throw together in a few hours, so might break at any point

## Not seeing any Analytics Events?

1. Make sure you've added the Analytics hostname by clicking the extension icon and entering it in the input field
