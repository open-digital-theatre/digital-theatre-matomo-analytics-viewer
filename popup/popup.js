document.addEventListener("DOMContentLoaded", () => {
  const currentHostElement = document.getElementById("currentHost");

  chrome.storage.sync.get(["analyticsHost"], function (result) {
    if (result.analyticsHost) {
      currentHostElement.textContent = result.analyticsHost;
      currentHostElement.style.color = "black";
    } else {
      currentHostElement.textContent = "NOT SET!";
      currentHostElement.style.color = "red";
    }
    document.getElementById("analyticsHost").value = result.analyticsHost || "";
  });

  document
    .getElementById("settingsForm")
    .addEventListener("submit", (event) => {
      event.preventDefault();
      let host = document.getElementById("analyticsHost").value.trim();
      host = host.replace(/^https?:\/\//, "");

      chrome.storage.sync.set({ analyticsHost: host }, () => {
        currentHostElement.textContent = host;
        currentHostElement.style.color = host ? "black" : "red";
      });
    });
  document.getElementById("clearHost").addEventListener("click", () => {
    chrome.storage.sync.remove("analyticsHost", () => {
      currentHostElement.textContent = "NOT SET!";
      currentHostElement.style.color = "red";
      document.getElementById("analyticsHost").value = "";
    });
  });
});
