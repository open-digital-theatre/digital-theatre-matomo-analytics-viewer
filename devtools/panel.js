let expandedStates = {};

const port = chrome.runtime.connect({ name: "matomoTrackerDevTools" });

const paramDescriptions = {
  c_n: "Content Name",
  c_p: "Content Piece",
  c_i: "Content Interaction Name",
  c_t: "Content Target",
  pf_net: "Network Performance",
  pf_srv: "Server Performance",
  pf_pag: "Page Performance",
  pf_dom: "DOM Performance",
  pf_tfr: "Transfer Performance",
  pf_dm1: "DOM Processing Time",
  pf_dm2: "DOM Completion Time",
  pf_onl: "Onload Time",
  e_c: "Event Category",
  e_a: "Event Action",
  e_n: "Event Name",
  e_v: "Event Value",
  ca: "Content Available",
  idsite: "Site ID",
  rec: "Record",
  r: "Cache Buster",
  h: "Hour",
  m: "Minute",
  s: "Second",
  url: "Current URL",
  uid: "User ID",
  _id: "Visitor ID",
  _idn: "New Visitor",
  send_image: "Send Image",
  _refts: "Referrer Timestamp",
  pdf: "PDF",
  qt: "QuickTime",
  realp: "Real Player",
  wma: "Windows Media",
  fla: "Flash",
  java: "Java",
  ag: "Age Group",
  cookie: "Cookie",
  res: "Screen Resolution",
  dimension1: "Master Account (Company) ID",
  dimension2: "Master Account (Company) Name",
  dimension3: "Organisation ID",
  dimension4: "Organisation Name and ID",
  dimension5: "Organisation Country",
  dimension6: "Organisation Level",
  dimension7: "Organisation Category",
  dimension8: "Content ID",
  dimension9: "Content Production Company",
  dimension10: "Content Type",
  dimension11: "Organisation Authentication Method",
  dimension12: "Subscription Type",
  dimension13: "Content Subtype",
  dimension14: "Card Title",
  dimension15: "Card Index (Position)",
  dimension16: "Duration Minutes",
  dimension17: "Clicked From Page Type",
  dimension18: "Clicked From Page Title",
  dimension19: "Clicked From Page Location",
  dimension20: "Clicked From URL",
  dimension21: "Played From Playlist",
  dimension22: "Education Level",
  dimension23: "Permission",
  pv_id: "Page View ID",
  uadata: "User Agent Data",
  action_name: "Action Name",
  urlref: "Referrer URL",
  fa_pv: "Fingerprinting Page View",
  "fa_fp[0][fa_vid]": "Fingerprinting Visitor ID 1",
  "fa_fp[0][fa_fv]": "Fingerprinting Visit Count 1",
  "fa_fp[1][fa_vid]": "Fingerprinting Visitor ID 2",
  "fa_fp[1][fa_fv]": "Fingerprinting Visit Count 2",
  ma_id: "Media Analytics ID",
  ma_ti: "Media Analytics Title",
  ma_pn: "Media Analytics Player Name",
  ma_mt: "Media Analytics Media Type",
  ma_re: "Media Analytics Resource",
  ma_st: "Media Analytics Start Time",
  ma_ps: "Media Analytics Pause Status",
  ma_le: "Media Analytics Length",
  ma_ttp: "Media Analytics Time to Play",
  ma_w: "Media Analytics Width",
  ma_h: "Media Analytics Height",
  ma_fs: "Media Analytics Fullscreen",
  ma_se: "Media Analytics Session",
};

function createCollapsibleSection(detail, index) {
  const section = document.createElement("div");
  const header = document.createElement("button");
  const content = document.createElement("div");
  const queryParams = detail.url ? parseQueryParams(detail.url) : {};
  const hasValidRequestBody = detail.requestBody && !detail.requestBody.error;
  const hasValidQueryParams = Object.keys(queryParams).length > 0;

  header.textContent = `${index + 1}. ${new Date(
    detail.timeStamp
  ).toLocaleString()} - ${detail.type}`;
  content.style.display = "none";

  header.style.background = "#e9e9e9";
  header.style.color = "#333";
  header.style.border = "1px solid #d3d3d3";
  header.style.padding = "10px";
  header.style.width = "calc(100% - 22px)";
  header.style.textAlign = "left";
  header.style.outline = "none";
  header.style.fontWeight = "bold";
  header.style.cursor = "pointer";
  header.style.borderRadius = "5px";
  header.style.marginBottom = "5px";
  content.style.display = "none";
  content.style.marginTop = "5px";

  header.addEventListener("click", () => {
    const eventId = `${detail.timeStamp}-${detail.type}`;
    expandedStates[eventId] = !expandedStates[eventId];
    content.style.display = expandedStates[eventId] ? "block" : "none";
  });

  const eventId = `${detail.timeStamp}-${detail.type}`;
  content.style.display = expandedStates[eventId] ? "block" : "none";
  console.log(`hasValidQueryParams: ${hasValidQueryParams}`);
  console.log(`hasValidRequestBody: ${hasValidRequestBody}`);
  console.log(`queryParams: ${JSON.stringify(queryParams)}`);
  console.log(`detail.requestBody: ${JSON.stringify(detail.requestBody)}`);
  if (hasValidQueryParams || hasValidRequestBody) {
    const table = document.createElement("table");
    if (hasValidQueryParams) {
      Object.entries(queryParams).forEach(([key, value]) => {
        const row = document.createElement("tr");
        const keyCell = document.createElement("td");
        const paramCell = document.createElement("td");
        const valueCell = document.createElement("td");
        keyCell.textContent = key;
        paramCell.textContent = paramDescriptions[key] || key;
        valueCell.textContent = value;
        row.appendChild(paramCell);
        row.appendChild(keyCell);
        row.appendChild(valueCell);
        table.appendChild(row);
      });
    }
    // Add request body if it's valid and not just an error message
    if (hasValidRequestBody) {
      const row = document.createElement("tr");
      const keyCell = document.createElement("td");
      const paramCell = document.createElement("td");
      const valueCell = document.createElement("td");
      keyCell.textContent = "Request Body";
      paramCell.textContent = paramDescriptions[key] || key;
      valueCell.textContent = JSON.stringify(detail.requestBody);
      row.appendChild(paramCell);
      row.appendChild(keyCell);
      row.appendChild(valueCell);
      table.appendChild(row);
    }
    content.appendChild(table);
  } else {
    content.textContent = "No query parameters found";
  }

  section.appendChild(header);
  section.appendChild(content);

  return section;
}

function fetchData() {
  port.postMessage({ action: "fetchData" });
}

function parseQueryParams(url) {
  const queryParams = {};
  const queryString = new URL(url).search.slice(1); // Remove '?' at the start
  const pairs = queryString.split("&");

  pairs.forEach((pair) => {
    const [key, value] = pair.split("=");
    queryParams[decodeURIComponent(key)] = decodeURIComponent(value || "");
  });

  return queryParams;
}

const renderedEvents = new Set();

function renderData(data) {
  const eventsElement = document.getElementById("events");

  data.forEach((detail, index) => {
    if (detail.type !== "script") {
      const eventId = `${detail.timeStamp}-${detail.type}`; // Adjust based on your data structure
      if (!renderedEvents.has(eventId)) {
        const section = createCollapsibleSection(detail, index);
        eventsElement.appendChild(section);
        renderedEvents.add(eventId);
      }
    }
  });
}

port.onMessage.addListener(function (message) {
  if (message.action === "sendData") {
    renderData(message.data);
  }
});
document.addEventListener("DOMContentLoaded", function () {
  // Existing fetchData and setInterval calls
  fetchData();
  setInterval(fetchData, 3000);

  // Add Clear Events Button functionality
  const clearEventsButton = document.createElement("button");
  clearEventsButton.textContent = "Clear All Events";
  clearEventsButton.style.marginBottom = "10px";
  clearEventsButton.addEventListener("click", () => {
    const eventsElement = document.getElementById("events");
    eventsElement.innerHTML = ""; // Clear the displayed events
    expandedStates = {}; // Reset the expanded states
    renderedEvents.clear(); // Clear the set of rendered events

    console.log("All events cleared");
  });

  clearEventsButton.addEventListener("click", () => {
    const eventsElement = document.getElementById("events");
    eventsElement.innerHTML = ""; // Clear the displayed events
    expandedStates = {}; // Reset the expanded states
    renderedEvents.clear(); // Clear the set of rendered events

    // Send message to background script to clear captured data
    port.postMessage({ action: "clearData" });

    console.log("All events cleared");
  });

  // Assuming your <h1> exists and has an ID or you can append next to the 'events' div
  document.body.insertBefore(
    clearEventsButton,
    document.getElementById("events")
  );
});
